﻿using Coscine.Api.Search.Helpers;
using Coscine.Api.Search.Models;
using Coscine.ApiCommons;
using Coscine.Configuration;
using Coscine.SemanticSearch.Clients;
using Coscine.SemanticSearch.Core;
using Coscine.SemanticSearch.Util.QueryObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Coscine.Api.Search.Controllers
{
    /// <summary>
    /// Controller for the search.
    /// </summary>
    [Authorize]
    public class SearchController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly Authenticator _authenticator;

        private readonly string[] _defaultLanguages = new string[] { "en", "de" };
        private readonly string _metadataStore;

        private readonly IRdfConnector _connector;
        private readonly ISearchClient _searchClient;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SearchController()
        {
            _configuration = Program.Configuration;
            _authenticator = new Authenticator(this, Program.Configuration);
            _metadataStore = _configuration.GetString("coscine/local/virtuoso/additional/url");
            var elasticsearchServer = _configuration.GetString("coscine/local/elasticsearch/additional/server");
            var elasticsearchPort = _configuration.GetString("coscine/local/elasticsearch/additional/port");
            _connector = new VirtuosoRdfConnector(_metadataStore);
            _searchClient = new ElasticsearchSearchClient(elasticsearchServer, elasticsearchPort);
        }

        /// <summary>
        /// Search Elasticsearch
        /// </summary>
        /// <param name="searchParameters"></param>
        /// <returns>Search results</returns>
        [HttpGet("[controller]/")]
        public async Task<ActionResult<IEnumerable<ItemSearchResult>>> SearchAsync([FromQuery] SearchParameters? searchParameters = null)
        {
            if (searchParameters is null)
            {
                searchParameters = new SearchParameters();
            }

            if (searchParameters.SearchQuery is null)
            {
                searchParameters.SearchQuery = "";
            }

            var currentUser = _authenticator.GetUser();
            if (searchParameters.IncludedLanguages?.Any() != true)
            {
                searchParameters.IncludedLanguages = _defaultLanguages;
            }

            try
            {
                var categoryCounts = new List<Category>();

                foreach (CategoryFilter category in Enum.GetValues(typeof(CategoryFilter)))
                {
                    var count = await Searchers.CountForItemsAsync(
                        currentUser.Id,
                        searchParameters.SearchQuery,
                        searchParameters.UseAdvancedSyntax,
                        includePrivateRecords: searchParameters.IncludeUsers,
                        _connector,
                        _searchClient,
                        searchParameters.IncludedLanguages.ToList(),
                        category);

                    categoryCounts.Add(new Category(category, category.ToString(), count));
                }

                // The  total count for the pagination depends on the selected category
                var totalCount = categoryCounts
                        .First(x => x.Id == searchParameters.CategoryFilter)
                        .Count;

                var items = await Searchers.SearchForItemsAsync(
                    currentUser.Id,
                    searchParameters.SearchQuery,
                    searchParameters.UseAdvancedSyntax,
                    includePrivateRecords: searchParameters.IncludeUsers,
                    _connector,
                    _searchClient,
                    searchParameters.IncludedLanguages.ToList(),
                    searchParameters.PageSize,
                    searchParameters.PageNumber - 1,
                    searchParameters.OrderBy,
                    searchParameters.CategoryFilter);

                var response = new PagedList<ItemSearchResult>(items, totalCount, searchParameters.PageNumber, searchParameters.PageSize);

                var metadata = new
                {
                    response.TotalCount,
                    response.PageSize,
                    response.CurrentPage,
                    response.TotalPages,
                    response.HasNext,
                    response.HasPrevious,
                    Categories = categoryCounts
                };

                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));

                return response;
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }

    internal record Category(CategoryFilter Id, string Name, int Count);
}