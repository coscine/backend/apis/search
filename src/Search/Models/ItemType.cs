﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Coscine.Api.Search.Models
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ItemType
    {
        Metadata,
        Project,
        Resource,
    }
}