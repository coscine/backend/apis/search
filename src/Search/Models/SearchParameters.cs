﻿using Coscine.SemanticSearch.Util.QueryObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Search.Models
{
    /// <summary>
    /// Parameters and filters for the search
    /// </summary>
    public class SearchParameters : QueryStringParameters
    {
        /// <summary>
        /// The actual search query
        /// </summary>
        public string SearchQuery { get; set; } = "*";

        /// <summary>
        /// Specify user or only public metadata records could be found
        /// </summary>
        public bool IncludeUsers { get; set; } = true;

        /// <summary>
        /// Set true for advanced Elasticsearch search syntax
        /// </summary>
        public bool UseAdvancedSyntax { get; set; }

        /// <summary>
        /// Set the used languages
        /// </summary>
        public IEnumerable<string>? IncludedLanguages { get; set; }

        /// <summary>
        /// Set the category filter
        /// </summary>
        [EnumDataType(typeof(CategoryFilter))]
        [JsonConverter(typeof(StringEnumConverter))]
        public CategoryFilter CategoryFilter { get; set; } = CategoryFilter.None;

        /// <summary>
        /// Set the order for sorting
        /// </summary>
        [EnumDataType(typeof(OrderBy))]
        [JsonConverter(typeof(StringEnumConverter))]
        public OrderBy OrderBy { get; set; } = OrderBy.Score_Desc;
    }
}