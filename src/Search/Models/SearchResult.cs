﻿using Coscine.Api.Search.Helpers;

namespace Coscine.Api.Search.Models
{
    /// <summary>
    /// Search result return object
    /// </summary>
    public class SearchResult
    {
        /// <summary>
        /// Search result files
        /// </summary>
        public PagedList<ItemSearchResult> Items { get; set; }
    }
}