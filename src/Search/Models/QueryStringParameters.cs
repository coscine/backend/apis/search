﻿namespace Coscine.Api.Search.Models
{
    /// <summary>
    /// Parameters for pagination of the search
    /// </summary>
    public class QueryStringParameters
    {
        private const int maxPageSize = 50;

        /// <summary>
        /// The requested page number
        /// </summary>
        public int PageNumber { get; set; } = 1;

        private int _pageSize = 10;

        /// <summary>
        /// The size of a page
        /// </summary>
        public int PageSize
        {
            get
            {
                return _pageSize;
            }
            set
            {
                _pageSize = (value > maxPageSize) ? maxPageSize : value;
            }
        }
    }
}