﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Search.Models
{
    public enum OrderBy
    {
        Date_Asc,
        Date_Desc,
        Score_Asc,
        Score_Desc,
        Name_Asc,
        Name_Desc,
    }
}