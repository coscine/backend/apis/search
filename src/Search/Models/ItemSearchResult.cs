﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace Coscine.Api.Search.Models;

/// <summary>
/// Item Search Result Object
/// </summary>
public class ItemSearchResult
{
    /// <summary>
    /// Graph Name
    /// </summary>
    [JsonProperty("graphName")]
    public Uri GraphName { get; set; }

    /// <summary>
    /// Graph Name
    /// </summary>
    [JsonProperty("type")]
    public ItemType Type { get; set; } = ItemType.Metadata;

    /// <summary>
    /// Search result fields
    /// </summary>
    [JsonProperty("source")]
    public JObject Source { get; set; }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="key">Search result entry key containing the resource id</param>
    /// <param name="value">Search result</param>
    public ItemSearchResult(string key, JObject value)
    {
        GraphName = new Uri(key);
        Source = value;
    }

    /// <summary>
    /// Method to convert the Semantic Search dictionary to a list of File Search Result Objects
    /// </summary>
    /// <param name="results"></param>
    /// <returns></returns>
    public static IEnumerable<ItemSearchResult> ParseResultsToList(IDictionary<string, JObject> results)
    {
        var output = new List<ItemSearchResult>();
        foreach (var entry in results)
        {
            var searchResult = new ItemSearchResult(entry.Key, entry.Value);
            if (entry.Value.ContainsKey("structureType"))
            {
                var type = entry.Value["structureType"]?.ToString();
                if (type == "https://purl.org/coscine/terms/structure#Metadata")
                {
                    searchResult.Type = ItemType.Metadata;
                }
                else if (type == "https://purl.org/coscine/terms/structure#Project")
                {
                    searchResult.Type = ItemType.Project;
                }
                else if (type == "https://purl.org/coscine/terms/structure#Resource")
                {
                    searchResult.Type = ItemType.Resource;
                }
            }
            output.Add(searchResult);
        }
        return output;
    }
}