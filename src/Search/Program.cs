﻿using Coscine.ApiCommons;
using Coscine.Configuration;

namespace Coscine.Api.Search
{
    /// <summary>
    /// Standard Program class.
    /// </summary>
    public class Program : AbstractProgram<ConsulConfiguration>
    {
        /// <summary>
        /// Standard Main method.
        /// </summary>
        static void Main()
        {
            InitializeWebService<Startup>();
        }
    }
}
