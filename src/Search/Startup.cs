﻿using Coscine.ApiCommons;
using Microsoft.Extensions.DependencyInjection;

namespace Coscine.Api.Search
{
    /// <summary>
    /// Standard Startup class.
    /// </summary>
    public class Startup : AbstractStartup
    {
        /// <summary>
        /// Standard Startup constructor
        /// </summary>
        public Startup()
        {
        }
    }
}