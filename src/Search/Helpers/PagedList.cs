﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Coscine.Api.Search.Helpers;

/// <summary>
/// Helper class for paged results
/// </summary>
/// <typeparam name="T"></typeparam>
public class PagedList<T> : List<T>
{
    /// <summary>
    /// The current page
    /// </summary>
    public int CurrentPage { get; }

    /// <summary>
    /// Total pages
    /// </summary>
    public int TotalPages { get; }

    /// <summary>
    /// The site of a page
    /// </summary>
    public int PageSize { get; }

    /// <summary>
    /// Total amount of results
    /// </summary>
    public int TotalCount { get; }

    /// <summary>
    /// Has a previous pages
    /// </summary>
    public bool HasPrevious => CurrentPage > 1;

    /// <summary>
    /// Has a following page
    /// </summary>
    public bool HasNext => CurrentPage < TotalPages;

    /// <summary>
    /// Create a paged list
    /// </summary>
    /// <param name="items"></param>
    /// <param name="count"></param>
    /// <param name="pageNumber"></param>
    /// <param name="pageSize"></param>
    public PagedList(IEnumerable<T> items, int count, int pageNumber, int pageSize)
    {
        TotalCount = count;
        PageSize = pageSize;
        CurrentPage = pageNumber;
        TotalPages = (int)Math.Ceiling(count / (double)pageSize);
        AddRange(items);
    }

    /// <summary>
    /// Create a paged list
    /// </summary>
    /// <param name="source"></param>
    /// <param name="pageNumber"></param>
    /// <param name="pageSize"></param>
    /// <returns></returns>
    // source should better be IQueryable
    public static PagedList<T> ToPagedList(IEnumerable<T> source, int pageNumber, int pageSize)
    {
        var count = source.Count();
        var items = source.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
        return new PagedList<T>(items, count, pageNumber, pageSize);
    }
}