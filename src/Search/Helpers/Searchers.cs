﻿using Coscine.Api.Search.Models;
using Coscine.SemanticSearch;
using Coscine.SemanticSearch.Core;
using Coscine.SemanticSearch.Util.QueryObjects;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Coscine.Api.Search.Helpers;

/// <summary>
/// Class implementing static searcher methods
/// </summary>
public class Searchers
{
    public static async Task<IEnumerable<ItemSearchResult>> SearchForItemsAsync(Guid userId, string searchQuery, bool advancedSearch, bool includePrivateRecords, IRdfConnector connector, ISearchClient searchClient, List<string> languages, int pageSize, int page, OrderBy orderBy, CategoryFilter categoryFilter)
    {
        var userIdentifier = includePrivateRecords ? userId.ToString() : null;
        var mapper = new RdfSearchMapper(connector, searchClient, languages);

        Sort? sort = null;

        switch (orderBy)
        {
            case OrderBy.Date_Asc:
                sort = new Sort { DateCreated = new DateCreated { Order = "asc" } };
                break;

            case OrderBy.Date_Desc:
                sort = new Sort { DateCreated = new DateCreated { Order = "desc" } };
                break;

            case OrderBy.Score_Asc:
                sort = new Sort { Score = new SortElement { Order = "asc" } };
                break;

            case OrderBy.Score_Desc:
                sort = new Sort { Score = new SortElement { Order = "desc" } };
                break;

            case OrderBy.Name_Asc:
                sort = new Sort { Name = new Script { Order = "asc" } };
                break;

            case OrderBy.Name_Desc:
                sort = new Sort { Name = new Script { Order = "desc" } };
                break;
        }

        var results = await mapper.SearchAsync(searchQuery, userIdentifier, advancedSearch, pageSize, pageSize * page, sort, categoryFilter);
        return ItemSearchResult.ParseResultsToList(results);
    }

    public static async Task<int> CountForItemsAsync(Guid userId, string searchQuery, bool advancedSearch, bool includePrivateRecords, IRdfConnector connector, ISearchClient searchClient, List<string> languages, CategoryFilter categoryFilter = CategoryFilter.None)
    {
        var userIdentifier = includePrivateRecords ? userId.ToString() : null;
        var mapper = new RdfSearchMapper(connector, searchClient, languages);
        return await mapper.CountAsync(searchQuery, userIdentifier, advancedSearch, categoryFilter);
    }
}